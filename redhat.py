import wolframalpha
import sys
import json 
import re
import requests
import yaml
import os
from inspect import getsourcefile

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]
conf = yaml.load(open(parent_dir+'/conf/application.yml'))

key = conf['thingspeak']['redhat']['key']
wolfram_key = conf['wolframalpha']['redhat']['key']
client = wolframalpha.Client(wolfram_key)

res = client.query('red hat stock price')
t = res['pod'][1]['subpod']['plaintext']

print(t)
m = re.search('[0-9]+.[0-9]+',t)

url = "https://api.thingspeak.com/update?api_key="+key+"&field1="+m.group(0)
r = requests.get(url)
