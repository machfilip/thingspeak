
from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json

import requests
import urllib2
import re
import sys
import json
import yaml
import sys
import os
from string import *
from decimal import Decimal, getcontext
from inspect import getsourcefile

getcontext().prec = 6

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]
conf = yaml.load(open(parent_dir+'/conf/application.yml'))

key = conf['thingspeak']['ltc']['key']

url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?id=1,2,1027,1720,1831'
headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': '77f12e34-d830-4060-8b19-7c635b53a59c',
}

session = Session()
session.headers.update(headers)

try:
  response = session.get(url)
  data = json.loads(response.text)
  ltc_price = data['data']['2']['quote']['USD']['price']

  str1 = "{:.5f}".format(ltc_price)  
  url = "https://api.thingspeak.com/update?api_key="+key+"&field1="+str1
  r = requests.get(url)

except (ConnectionError, Timeout, TooManyRedirects) as e:
  print(e)
