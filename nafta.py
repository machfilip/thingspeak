import urllib
import requests
import urllib3
import urllib3.request
#import urllib3.parse
from bs4 import BeautifulSoup
import re
import sys
import yaml
import sys
import os
from inspect import getsourcefile
import ssl

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]
conf = yaml.load(open(parent_dir+'/conf/application.yml'))

key = conf['thingspeak']['nafta']['key']

url = 'http://www.tank-ono.cz/cz/index.php?page=cenik'
context = ssl._create_unverified_context()

with urllib.request.urlopen(url, context=context) as response:
   html = response.read()

soup = BeautifulSoup(html, "html.parser")

links = soup.find('table', attrs={'class', 'cenik'})
rows = links.find_all('tr')

for item in rows:
  cols = item.find_all('td')
  if len(cols) == 11:
    if cols[0].get_text().find('ČS Vysoké Mýto') != -1 :
      print(cols[0].get_text().strip() )
      price = cols[5].get_text()
      price = price.replace(",",".")
      print(price)
      url = "https://api.thingspeak.com/update?api_key="+key+"&field1="+price
      r = requests.get(url)

