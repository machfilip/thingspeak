# -*- coding: utf-8 -*-
import urllib
from urllib.parse import urlparse
import urllib.request
from bs4 import BeautifulSoup
import sys
import os
from datetime import datetime
import syslog
import ssl
import errno
import yaml
from inspect import getsourcefile
syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)
import requests

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]
conf = yaml.load(open(parent_dir+'/conf/application.yml'))

key = conf['thingspeak']['ibm']['key']

url = 'https://markets.businessinsider.com/stocks/ibm-stock'
context = ssl._create_unverified_context()

with urllib.request.urlopen(url, context=context) as response:
   html = response.read()

soup = BeautifulSoup(html, "html.parser")

links = soup.find('span', attrs={'class', 'price-section__current-value'}).get_text()
url = "https://api.thingspeak.com/update?api_key="+key+"&field1="+links
r = requests.get(url)

print(links)

sys.exit()
