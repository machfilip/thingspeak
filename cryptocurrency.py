from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json

import requests
import urllib2
import re
import sys
import json
import yaml
import sys
import os
from string import *
from decimal import Decimal, getcontext
from inspect import getsourcefile

getcontext().prec = 6

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]
conf = yaml.load(open(parent_dir+'/conf/application.yml'))

key_crypto = conf['thingspeak']['cryptocurrency']['key']

url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?id=1,2,1027,1720,6636'
headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': '77f12e34-d830-4060-8b19-7c635b53a59c',
}

session = Session()
session.headers.update(headers)

try:
  response = session.get(url)
  data = json.loads(response.text)
  iota_price  = data['data']['1720']['quote']['USD']['price']
  btc_price   = data['data']['1']['quote']['USD']['price']
  ltc_price   = data['data']['2']['quote']['USD']['price']
  eth_price   = data['data']['1027']['quote']['USD']['price']
  polka_price = data['data']['6636']['quote']['USD']['price']
  
  iota_price = "{:.3f}".format(iota_price)
  btc_price = "{:.2f}".format(btc_price)
  ltc_price = "{:.2f}".format(ltc_price)
  eth_price = "{:.2f}".format(eth_price)
  polka_price = "{:.3f}".format(polka_price)

  url = "https://api.thingspeak.com/update?api_key="+key_crypto+"&field1="+btc_price+"&field2="+ltc_price+"&field3="+eth_price+"&field4="+iota_price+"&field5="+polka_price
  r = requests.get(url)

except (ConnectionError, Timeout, TooManyRedirects) as e:
  print(e)
