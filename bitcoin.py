import requests
import urllib2
import re
import sys
from urlparse import urlparse
import json
import yaml
import sys
import os
from inspect import getsourcefile
import ssl
ssl.match_hostname = lambda cert, hostname: True

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]
conf = yaml.load(open(parent_dir+'/conf/application.yml'))

key = conf['thingspeak']['bitcoin']['key']

url = 'https://api.coindesk.com/v1/bpi/currentprice/BTC.json'

req = urllib2.Request(url, headers={ 'User-Agent': 'Mozilla/5.0' })

try:
    page = urllib2.urlopen(req)
except urllib2.HTTPError, e:
    print e.fp.read()

html = page.read()

data = json.loads(html)

str = data['bpi']['USD']['rate']
str = str.replace(",", "")

url = "https://api.thingspeak.com/update?api_key="+key+"&field1="+str
r = requests.get(url)
